---
title: How to start adopting Conversational Development
date:   2017-04-13 8:00:00 +0100
categories: guide beginner
---

Adopting a new process can be a daunting task for any team. The key to success is getting everyone on-board and taking on change in a manner that is comfortable for all. In this guide we will cover how to assess your own process and introduce the four principles of Conversational Development into it.

### Assess your current process

The first step on the path to adopting Conversation Development (ConvDev) is determining your team's or organization's place on the [Maturity Model](/maturity-model) to get an idea of your process maturity, possible problems, and potential for growth.

Each of the five stages on the model has fewer barriers to collaboration and invites more of the extended team to be part of the process. Find your place on the model and set goals for your team to get to the next stage. If your are missing one aspect don't jump ahead, make getting their part of your goal.

### Get familiar with ConvDev principles

1. [**Shorten** the conversation cycle](/shorten-cycle)
1. [**Thread** the conversation through all stages](/thread-conversations) 
1. [**Open** conversations without consensus](/open-conversations)
1. [**Result** oriented conversation](/results-oriented) 

### Focus on Minimum Viable Change

The objective of software development is to produce value as soon as possible. Instead of minimum viable products or minimum viable features, start thinking in terms of Minimum Viable Changes - the smallest change that can be shipped and measured.

<div class='col-md-12 col-xs-12'>
  <p>
    <img style="width:100%" src="/images/mvc.png" />
  </p>
</div>

By focusing on shipping Minimum Viable Changes instead of entire feature sets or at arbitrary time intervals your team will be in a better position to measure and assess value and create a result oriented culture.

#### 1. Shorten

As an exercise to get started pick a feature and ask your team what is the smallest amount of change they think is needed to get a working version shipped For example instead of building a full-featured issue tracking solutions with issue boards and reporting start with a simple to do list and gradually expand it.

#### 2. Thread

Record any meetings or if not possible have someone take meeting notes to share with everyone afterwards. It is important to thread the conversation through the entire process either by linking various tools or using a centralized solution. A linked conversation with clearly defined stage transitions will allow your team to measure the entire cycle time for the minimum viable change as well as the time spent in each stage e.g design, development, testing etc. 

#### 3. Open

Make sure you invite someone from each cross functional team to the conversation and that the conversation is not dominated by anyone. There is nothing wrong with having a project manager or agile coach lead the conversation and act as a mediator.

#### 4. Results

Tracking the cycle time will allow your team to become more result orientated by optimizing the first three principles. Once you have finished your first cycle begin again with the team thinking about the next minimum viable change to make.  

In the next post we will dive into more detail on how to shorten your conversation cycle.
